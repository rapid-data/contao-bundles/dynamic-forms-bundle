# Contao Dynamic Forms Bundle
Stellt Contao-Formular-Generator Formulare als mehrseitige, dynamische JavaScript-Formulare zur Verfügung.

## Contao-Formular Replacement
Wird im Backend anstelle des Formular-Includes verwendet und bietet die Möglichkeit, eine Versandbestätigung nach Abschicken
des Formulars anzuzeigen.
![img.png](docs/img.png)

## Mehrseitige Formulare anlegen
Nach der Installation können im Contao-Formulargenerator Elemente des Typs „Seitenumbruch” angelegt werden.
In diesen lassen sich dann auch der Seitentitel und die Beschriftung der Weiter/Zurück festlegen.
![img.png](docs/page-break.png)

## Breadcrumb im Lieferumfang enthalten
Auf der gleichen Seite wie das Formular kann ebenfalls eine Breadcrumb/Step-Navigation für mehrseitige Formulare angezeigt werden
![img.png](docs/breadcrumb_backend.png)

Step-Navigation:
![img.png](docs/breadcrumb_stepper.png)

Breadcrumb:
![img.png](docs/breadcrumb_breadcrumb.png)
