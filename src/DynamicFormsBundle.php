<?php
declare(strict_types = 1);

namespace RapidData\ContaoDynamicFormsBundle;

use RapidData\ContaoDynamicFormsBundle\DependencyInjection\DynamicFormsExtension;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class DynamicFormsBundle extends Bundle
{
    public function getContainerExtension(): DynamicFormsExtension
    {
        return new DynamicFormsExtension();
    }
}
