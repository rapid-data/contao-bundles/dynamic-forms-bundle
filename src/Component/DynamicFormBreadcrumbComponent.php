<?php
declare(strict_types=1);

namespace RapidData\ContaoDynamicFormsBundle\Component;

use Contao\CoreBundle\Framework\ContaoFramework;
use Contao\FormFieldModel;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use RoflCopter24\SymfonyLivewireBundle\Annotation\Livewire;
use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireRequestData;
use RoflCopter24\SymfonyLivewireBundle\Service\ComponentServicesInterface;
use Twig\TemplateWrapper;

/**
 * Class DynamicFormBreadcrumbComponent
 * @package App\Component
 * @Livewire("dynamic-form-breadcrumb")
 */
class DynamicFormBreadcrumbComponent extends LivewireComponent
{
    public int $formId;
    public int $currentPageIndex = 0;
    public string $mode = 'breadcrumb';
    public string $cssID = '';
    public string $classes = '';

    private Collection $formPages;

    public function __construct(ComponentServicesInterface $componentServices, ContaoFramework $framework)
    {
        parent::__construct($componentServices);
        if (!$framework->isInitialized()) {
            $framework->initialize(true);
        }
        $this->listeners = [
            'form.onPageChanged' => 'onPageChanged'
        ];
        $this->formPages = new Collection();
    }

    public function mount(int $formId, string $cssID = '', string $classes = '', string $mode = 'breadcrumb'): void
    {
        $this->formId = $formId;
        $this->formPages = $this->getFormPages($formId);
        $this->mode = $mode;
        $this->cssID = $cssID;
        $this->classes = $classes;

        $this->renderContext['formPages'] = $this->formPages;
        $this->renderContext['cssID'] = $this->cssID;
        $this->renderContext['classes'] = $this->classes;
    }

    public function hydrate(LivewireRequestData $requestData): void
    {
        $this->formPages = $this->getFormPages($this->formId);

        $this->renderContext['formPages'] = $this->formPages;
        $this->renderContext['cssID'] = $this->cssID;
        $this->renderContext['classes'] = $this->classes;
    }

    public function render(): TemplateWrapper
    {
        $data = array_merge($this->getPublicPropertiesDefinedBySubClass(), $this->renderContext);
        if ($this->mode === 'progressbar')
            return $this->view('@DynamicForms/livewire/dynamic-form-progressbar.html.twig', $data);

        return $this->view('@DynamicForms/livewire/dynamic-form-breadcrumb.html.twig', $data);
    }

    public function onPageChanged(int $formId, int $newPageIndex): void
    {
        if ($this->formId != $formId) {
            return;
        }

        $this->currentPageIndex = $newPageIndex;
    }

    private function getFormPages(int $formId): Collection
    {
        $fieldModels = FormFieldModel::findPublishedByPid($formId)->getModels();
        $pageCount = 0;
        return collect($fieldModels)
            ->chunkWhile(function(FormFieldModel $model, $key, $chunk) {
                // if the last element of the previous chunk is of type 'page-break' or 'mp_form_pageswitch', start a new chunk.
                return !Str::is(['page-switch', 'mp_form_pageswitch'], $chunk->last()->type);
            })
            ->map(function (Collection $collection) use (&$pageCount){
                $pageCount++;
                $pageTitle = $collection->last()->pageTitle;
                return $pageTitle && $pageTitle !== '' ? $pageTitle : 'Seite '.$pageCount;
            });
    }
}
