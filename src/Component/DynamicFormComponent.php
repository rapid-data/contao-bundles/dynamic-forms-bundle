<?php
declare(strict_types=1);

namespace RapidData\ContaoDynamicFormsBundle\Component;

use Contao\Controller;
use Contao\CoreBundle\Exception\InternalServerErrorException;
use Contao\CoreBundle\Framework\ContaoFramework;
use Contao\FormFieldModel;
use Contao\FormModel;
use Contao\PageModel;
use Contao\StringUtil;
use Contao\Widget;
use Illuminate\Support\Collection;
use Illuminate\Support\Str;
use Psr\Cache\InvalidArgumentException;
use Psr\Log\LoggerInterface;
use RapidData\ContaoDynamicFormsBundle\DynamicForm;
use RoflCopter24\SymfonyLivewireBundle\Annotation\Livewire;
use RoflCopter24\SymfonyLivewireBundle\Component\LivewireComponent;
use RoflCopter24\SymfonyLivewireBundle\Entity\LivewireRequestData;
use RoflCopter24\SymfonyLivewireBundle\Service\ComponentServicesInterface;
use stdClass;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\Cache\TagAwareCacheInterface;
use Twig\TemplateWrapper;

/**
 * Class DynamicFormComponent
 * @package App\Component
 * @Livewire("dynamic-form")
 */
class DynamicFormComponent extends LivewireComponent
{
    private Collection $formFields; // Hier werden die von Contao vorgerenderten Widgets gelagert

    public string $cssID;
    public string $class;
    public int $formId;
    public string $formName;
    public mixed $pageId;
    public array $fieldValues; // Speicher für die Values der Form-Felder
    public int $currentPage = 0;
    public bool $hasErrors = false;
    public string $successMessage = '';
    public bool $showSuccessMessage = false;

    private DynamicForm $dummyForm;
    private TagAwareCacheInterface $tagAwareCache;
    private ?Request $request;
    private LoggerInterface $logger;

    public function __construct(ComponentServicesInterface $services, ContaoFramework $framework, TagAwareCacheInterface $tagAwareCache, RequestStack $requestStack, LoggerInterface $logger)
    {
        parent::__construct($services);
        $logger->debug('[DynamicForm] Initializing.');

        if (!$framework->isInitialized()) {
            $framework->initialize(true);
        }
        $this->tagAwareCache = $tagAwareCache;
        $this->request = $requestStack->getCurrentRequest();

        // Auf die links genannten Events lauschen und wenn eines auftaucht,
        // die Methode rechts ausführen.
        $this->listeners = [
            'form.nextPage' => 'nextPage',
            'form.previousPage' => 'previousPage',
            'form.setPage' => 'setPage'
        ];

        $this->logger = $logger;
        $this->errorBag = $this->getErrorBag(); // Fehlerbeutel zur Anzeige von Fehlern im Frontend
        $this->renderContext['errorBag'] = $this->getErrorBag();

        $logger->debug('[DynamicForm] End of initialization.');
    }

    /**
     * Wird beim Laden der Contao-Seite aufgerufen
     * @param int $contaoFormId Die numerische Id des Contao-Formulars
     * @param string|null $successMessage Die Erfolgsnachricht, wie beim AJAX-Formular
     * @param string $cssID Die ID des Form-Elements
     * @param string $class CSS-Klasse des Form-Elements
     * @throws InvalidArgumentException
     */
    public function mount(int $contaoFormId, ?string $successMessage, string $cssID = '', string $class = ''): void
    {
        $this->logger->debug(sprintf('[DynamicForm] Component mount for Form %s: %s - %s - %s', (string)$contaoFormId, $successMessage, $cssID, $class));
        $this->formId = $contaoFormId;
        $this->pageId = $this->request->get('pageModel');
        $this->successMessage = Controller::replaceInsertTags($successMessage ?? 'Formular erfolgreich übertragen.', false);
        $this->cssID = $cssID;
        $this->class = $class;

        $form = FormModel::findByPk($contaoFormId);
        if (!$form) {
            $this->logger->error("[DynamicForm] Contao form with id $contaoFormId not found.");
            throw new InternalServerErrorException("Contao form with id $contaoFormId not found.");
        }

        if ($form->formID) {
            $this->formName = $form->formID;
        }

        [$cssID, $class] = StringUtil::deserialize($form->attributes);
        if ($cssID) {
            $this->cssID = $cssID;
        }
        if ($class) {
            $this->class = $class;
        }

        $this->dummyForm = $this->createDummyForm(); // Fake Contao-Form, wird für die Contao-Hooks benötigt
        $this->formFields = $this->getFormFieldsChunked($contaoFormId); // Formular Felder ermitteln und an den Umbrüchen teilen
        $this->fieldValues = $this->getInitialFormFieldValues(collect(FormFieldModel::findPublishedByPid($contaoFormId)));

        // Form field cache beim initialen Render löschen, sodass nichts mehr von vorherigen Sitzungen übrig bleibt.
        $this->tagAwareCache->delete('dynamic-form-'.$contaoFormId);
        // Versuchen die Form-Felder aus dem Cache abzurufen und falls diese nicht existieren, von Contao generieren lassen.
        $this->renderContext['formFields'] = $this->tagAwareCache->get('dynamic-form-'.$contaoFormId, function() { return $this->compileFormFields(); });
        $this->logger->debug('[DynamicForm] Mount finished.');
    }

    /**
     * Wird jedes Mal aufgerufen, wenn der Formular-Zustand sich ändert (sowohl beim ersten Laden, also auch bei AJAX-Requests).
     * @return TemplateWrapper
     */
    public function render(): TemplateWrapper
    {
        return $this->view('@DynamicForms/livewire/dynamic-form.html.twig', array_merge(
            $this->renderContext,
            $this->getPublicPropertiesDefinedBySubClass()
        ));
    }

    /**
     * Wird bei Eintreffen eines AJAX-Requests („Nachfolge-Request“) aufgerufen
     * @param LivewireRequestData $requestData
     */
    public function hydrate(LivewireRequestData $requestData): void
    {
        $this->logger->debug('[DynamicForm] Hydrating component.');
        $this->formFields = $this->getFormFieldsChunked($this->formId);
        $this->dummyForm = $this->createDummyForm(); // needed for the hooks

        $this->setupContaoInteropForSubsequent();
        $this->logger->debug('[DynamicForm] Hydration finished.');
    }

    public function nextPage(string $formId): void
    {
        $this->logger->debug('[DynamicForm] NextPage requested.');

        // Alle Events des Typs kommen hier an, uns interessieren allerdings nur die mit der ID des Formulars.
        if ($this->formId !== (int)$formId && $this->formName !== $formId) {
            $this->logger->debug("[DynamicForm] NextPage: This is not for us - we have the ID $this->formId not $formId");
            return;
        }
        $this->logger->debug("[DynamicForm] NextPage: This for us - we have the ID $this->formId!");

        $this->request->request->add($this->fieldValues); // set the form values as POST data on the request
        $count = $this->formFields->count();

        $this->logger->debug("[DynamicForm] NextPage: Running Contao-Validation on form field values.");
        if ($this->contaoValidateFoundErrors()) {

            $this->logger->debug("[DynamicForm] NextPage: Contao-Validation found errors. Returning early.");
            return;
        }
        $this->logger->debug("[DynamicForm] NextPage: Contao-Validation succeeded.");

        if ($this->currentPage >= $count-1) {
            $this->logger->debug("[DynamicForm] NextPage: This is the last page. Submitting form.");
            $this->submitForm();
            $this->logger->debug("[DynamicForm] NextPage: Form submitted. Returning.");
            return;
        }

        $this->logger->debug("[DynamicForm] NextPage: Advancing to page " . ($this->currentPage + 1) . '.');
        $this->currentPage = $this->currentPage + 1;

        $this->logger->debug("[DynamicForm] NextPage: afterPageChange.");
        $this->afterPageChange();
        $this->logger->debug("[DynamicForm] NextPage: Done.");
    }

    public function previousPage(string $formId): void
    {
        $this->logger->debug('[DynamicForm] PreviousPage requested.');

        // Alle Events des Typs kommen hier an, uns interessieren allerdings nur die mit der ID des Formulars.
        if ($this->formId !== (int)$formId && $this->formName !== $formId) {
            $this->logger->debug("[DynamicForm] PreviousPage: This is not for us - we have the ID $this->formId not $formId");
            return;
        }
        $this->logger->debug("[DynamicForm] PreviousPage: This for us - we have the ID $this->formId!");

        $this->request->request->add($this->fieldValues); // set the form values as POST data on the request
        $this->fieldValues['captcha'] = '';

        if ($this->formFields->count() <= 0) {
            $this->logger->debug("[DynamicForm] PreviousPage: This is the first page. Doing nothing.");
            return;
        }

        // dont check validity if the user goes back
        $this->logger->debug("[DynamicForm] PreviousPage: Advancing to page " . ($this->currentPage + 1) . '.');
        $this->currentPage--;

        $this->logger->debug("[DynamicForm] PreviousPage: afterPageChange.");
        $this->afterPageChange();
        $this->logger->debug("[DynamicForm] PreviousPage: Done.");
    }

    public function setPage(string $formId, int $pageNoZeroBased): void
    {
        $this->logger->debug('[DynamicForm] SetPage requested.');

        // Alle Events des Typs kommen hier an, uns interessieren allerdings nur die mit der ID des Formulars.
        if ($this->formId !== (int)$formId && $this->formName !== $formId) {
            $this->logger->debug("[DynamicForm] SetPage: This is not for us - we have the ID $this->formId not $formId");
            return;
        }
        $this->logger->debug("[DynamicForm] SetPage: This for us - we have the ID $this->formId!");

        $this->request->request->add($this->fieldValues); // set the form values as POST data on the request
        if ($this->formFields->count() <= $pageNoZeroBased || $pageNoZeroBased < 0) {
            return;
        }

        $this->fieldValues['captcha'] = '';

        $this->logger->debug("[DynamicForm] SetPage: Running Contao-Validation on form field values.");
        if ($this->contaoValidateFoundErrors()) {
            $this->logger->debug("[DynamicForm] SetPage: Contao-Validation found errors. Returning early.");
            return;
        }
        $this->logger->debug("[DynamicForm] SetPage: Contao-Validation succeeded.");

        $this->logger->debug("[DynamicForm] SetPage: Advancing to page " . $pageNoZeroBased . '.');
        $this->currentPage = $pageNoZeroBased;

        $this->logger->debug("[DynamicForm] SetPage: afterPageChange.");
        $this->afterPageChange();
        $this->logger->debug("[DynamicForm] SetPage: Done.");
    }

    private function submitForm(): void
    {
        $this->logger->debug("[DynamicForm] SubmitForm called.");
        $arrSubmitted = $this->fieldValues;
        $this->logger->debug("[DynamicForm] SubmitForm: Values are: ". http_build_query($arrSubmitted, '', ', '));
        $arrFields = $this->formFields
            ->flatten() // Verschachtelungen entfernen
            ->mapWithKeys(function (FormFieldModel $fieldModel) {
                return [$fieldModel->name => $fieldModel]; // Nach dem Schema "feldName" => fieldModel umbauen.
            })
            ->toArray();
        $arrLabels = $this->formFields
            ->flatten()
            ->mapWithKeys(function (FormFieldModel $fieldModel) {
                return [$fieldModel->name => $fieldModel->label];
            })
            ->toArray();

        try {
            $this->logger->debug("[DynamicForm] Calling internal Contao-Form processing.");
            $processFormData = new \ReflectionMethod(DynamicForm::class, 'processFormData');
            $processFormData->setAccessible(true);
            $processFormData->invokeArgs($this->dummyForm, [$arrSubmitted, $arrLabels, $arrFields]);

            $this->logger->debug("[DynamicForm] Success, Contao-Form processing succeeded. Firing onSubmitted event.");

            $this->emit('form.onSubmitted', $this->formId, $this->fieldValues);
            $this->showSuccessMessage = true;
            $this->logger->debug("[DynamicForm] Form successfully submitted.");
        } catch (\Exception $exception) {
            // @phpstan-ignore-next-line
            $this->logger->error('[DynamicForm] Form submit for dynamic form with id '.$this->formId.' failed: '.$exception->getResponse(), $exception->getTrace());
            $this->getErrorBag()->add('error', 'Formular konnte nicht abgeschickt werden.');
            $this->emit('form.onSubmitFailed', $this->formId, $exception);
        }
    }

    private function afterPageChange(): void
    {
        $this->tagAwareCache->delete('dynamic-form-'.$this->formId);
        $this->renderContext['formFields'] = $this->tagAwareCache->get('dynamic-form-'.$this->formId, function() { return $this->compileFormFields(); });

        $this->emit('form.onPageChanged', $this->formId, $this->currentPage);
    }

    private function setupContaoInteropForSubsequent(): void
    {
        // interop for contao internals
        $this->request->request->add($this->fieldValues); // set the form values as POST data on the request
        $this->request->attributes->set('pageModel', $this->pageId); // set the page id
        $this->request->attributes->set('_scope', 'frontend'); // make sure the request is seen as a frontend request.
        global $objPage;
        $objPage = PageModel::findByPk($this->pageId)->loadDetails(); // make the global page model available
    }

    /**
     * Returns the Contao FormFieldModels of the form chunked by page
     * @param int $contaoFormId The id of the form to retrieve the chunked fields for.
     * @return Collection The pages containing their respective FormFieldModels.
     */
    private function getFormFieldsChunked(int $contaoFormId): Collection
    {
        $fieldModels = FormFieldModel::findPublishedByPid($contaoFormId)->getModels();
        return collect($fieldModels)
            ->chunkWhile(function(FormFieldModel $model, $key, $chunk) {
                // if the last element of the previous chunk is of type 'page-break' or 'mp_form_pageswitch', start a new chunk.
                return !Str::is(['page-switch', 'mp_form_pageswitch'], $chunk->last()->type);
            });
    }

    private function getInitialFormFieldValues(Collection $formFields): array
    {
        return $formFields
            // filter all fields without name (fieldsets, buttons, etc.)
            ->filter(function (FormFieldModel $model) {
                return $model->name && $model->name !== '';
            })
            // bring them from the models Classes into a string dictionary $name => $value form
            ->mapWithKeys(function (FormFieldModel $model) {
                return [$model->name => Controller::replaceInsertTags($model->value)];
            })
            ->toArray();
    }

    private function compileFormFields(bool $checkValidity = false): array
    {
        $formFieldsForCurrentPage = $this->formFields->get($this->currentPage);

        // HOOK: compile form fields
        if (isset($GLOBALS['TL_HOOKS']['compileFormFields']) && \is_array($GLOBALS['TL_HOOKS']['compileFormFields']) && count($GLOBALS['TL_HOOKS']['compileFormFields']) > 0)
        {
            foreach ($GLOBALS['TL_HOOKS']['compileFormFields'] as $callback)
            {
                $service = $this->getHookServiceOrInstance($callback[0]);

                $arrFields = $service->{$callback[1]}($formFieldsForCurrentPage->all(), (string)$this->formId, $this->dummyForm);

                $formFieldsForCurrentPage = collect($arrFields);
            }
        }


        $row = 0;
        $max_row = $formFieldsForCurrentPage->count();
        return $formFieldsForCurrentPage
            // @phpstan-ignore-next-line
            ->map(function (FormFieldModel $objField) use ($formFieldsForCurrentPage, $row, $max_row, $checkValidity) {
                return $this->compileFormField($objField, $row, $max_row, $checkValidity);
            })
            ->toArray();
    }

    private function compileFormField(FormFieldModel $objField, &$row, &$max_row, bool $checkValidity): string
    {
        $this->logger->debug('[DynamicForm] CompileFormField called for '. $objField->id);
        // !--- Stolen from the contao-core-bundle form code ----
        $strClass = $GLOBALS['TL_FFL'][$objField->type] ?? '';

        // Continue if the class is not defined
        if (!class_exists($strClass))
        {
            return '';
        }

        $arrData = $objField->row();
        $arrData['decodeEntities'] = true;
        //$arrData['allowHtml'] = $this->allowTags;
        $arrData['rowClass'] = 'row_' . $row . (($row == 0) ? ' row_first' : (($row == ($max_row - 1)) ? ' row_last' : '')) . ((($row % 2) == 0) ? ' even' : ' odd');

        // Increase the row count if its a password field
        if ($objField->type == 'password')
        {
            ++$row;
            ++$max_row;

            $arrData['rowClassConfirm'] = 'row_' . $row . (($row == ($max_row - 1)) ? ' row_last' : '') . ((($row % 2) == 0) ? ' even' : ' odd');
        }

        // Submit buttons do not use the name attribute
        if ($objField->type == 'submit')
        {
            $arrData['name'] = '';
        }

        // Unset the default value depending on the field type (see #4722)
        if (!empty($arrData['value']) && !\in_array('value', StringUtil::trimsplit('[,;]', $GLOBALS['TL_DCA']['tl_form_field']['palettes'][$objField->type] ?? '')))
        {
            $arrData['value'] = '';
        }

        /** @var Widget $objWidget */
        $objWidget = new $strClass($arrData);
        $objWidget->required = (bool)$objField->mandatory;
        $objWidget->useRawRequestData = true; // make sure the form field values are retrieved from the Request object and not some other obscure way.

        // if a name is set for the form field, include the livewire model binding to fieldValues array with the field name as key.
        if ($objField->name) {
            $objWidget->addAttribute('wire:model.defer', "fieldValues." . $objField->name);
        }


        $this->logger->debug('[DynamicForm] CompileFormField: Running hooks.');
        // HOOK: load form field callback
        if (isset($GLOBALS['TL_HOOKS']['loadFormField']) && \is_array($GLOBALS['TL_HOOKS']['loadFormField']))
        {
            foreach ($GLOBALS['TL_HOOKS']['loadFormField'] as $callback)
            {
                $service = $this->getHookServiceOrInstance($callback[0]);
                // call the method or closure
                $objWidget = $service->{$callback[1]}($objWidget, (string)$this->formId, $arrData, $this->dummyForm);
            }
        }
        $this->logger->debug('[DynamicForm] CompileFormField: Hooks completed.');

        // Validate the input
        if ($checkValidity)
        {
            $this->logger->debug('[DynamicForm] CompileFormField: Checking validity using internal validation.');
            $objWidget->validate();

            $this->logger->debug('[DynamicForm] CompileFormField: Running validation hooks.');
            // HOOK: validate form field callback
            if (isset($GLOBALS['TL_HOOKS']['validateFormField']) && \is_array($GLOBALS['TL_HOOKS']['validateFormField']))
            {
                foreach ($GLOBALS['TL_HOOKS']['validateFormField'] as $callback)
                {
                    $service = $this->getHookServiceOrInstance($callback[0]);
                    $objWidget = $service->{$callback[1]}($objWidget, (string)$this->formId, $arrData, $this->dummyForm);
                }
            }
            $this->logger->debug('[DynamicForm] CompileFormField: Validation Hooks done.');

            if ($objWidget->hasErrors())
            {
                $this->hasErrors = true;
                $this->logger->debug('[DynamicForm] CompileFormField: Validation returned errors. Field name is '.$objWidget->name);
            }

            // Store current value in the session
            elseif ($objWidget->submitInput())
            {
                $this->logger->debug("[DynamicForm] CompileFormField: Submit input $objWidget->name => $objWidget->value");
                $_SESSION['FORM_DATA'][$objField->name] = $objWidget->value;
            }

            $this->logger->debug('[DynamicForm] CompileFormField: Validation end');
        }

        ++$row;
        $this->logger->debug('[DynamicForm] CompileFormField: Replace InsertTags and return.');
        return Controller::replaceInsertTags($objWidget->parse());
    }

    /**
     * Creates a dummy form instance that is needed for the hooks.
     *
     * @return DynamicForm
     */
    private function createDummyForm(): DynamicForm
    {
        $form = new stdClass();
        $form->form = $this->formId;
        // @phpstan-ignore-next-line
        return new DynamicForm($form);
    }

    /**
     * @param $value
     * @return object
     */
    private function getHookServiceOrInstance($value): object
    {
        if (is_string($value)) {
            // classname
            $service = $this->services->get($value);

            if (!$service) {
                throw new ServiceNotFoundException($value);
            }
        } else {
            // probably instance
            $service = $value;
        }
        return $service;
    }

    private function contaoValidateFoundErrors(): bool
    {
        $this->hasErrors = false; // reset error flag
        $this->tagAwareCache->delete('dynamic-form-'.$this->formId); // delete the cached form field html
        // render form fields + validate
        $this->renderContext['formFields'] = $this->tagAwareCache->get('dynamic-form-'.$this->formId, function() { return $this->compileFormFields(true); });
        return $this->hasErrors;
    }

}
