<?php
declare(strict_types=1);

namespace RapidData\ContaoDynamicFormsBundle;

use Contao\Form;

class DynamicForm extends Form
{
    /**
     * Override the reload method and do nothing.
     */
    public static function reload()
    {
        // do nothing
    }
}
