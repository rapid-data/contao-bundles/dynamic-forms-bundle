<?php
declare(strict_types=1);

namespace RapidData\ContaoDynamicFormsBundle\ContentElement;

use Contao\ContentModel;
use Contao\CoreBundle\Controller\ContentElement\AbstractContentElementController;
use Contao\CoreBundle\Routing\ScopeMatcher;
use Contao\StringUtil;
use Contao\Template;
use RoflCopter24\SymfonyLivewireBundle\Manager\LifecycleManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Contao\CoreBundle\ServiceAnnotation\ContentElement;
use Twig\Environment;

/**
 * Class FormElement
 * A wrapper for using a Livewire Component as a form
 * @package App\ContentElement
 * @ContentElement(category="includes")
 */
class FormElement extends AbstractContentElementController
{
    private LifecycleManager $lifecycleManager;
    private ScopeMatcher $scopeMatcher;
    private Environment $twig;

    public function __construct(LifecycleManager $lifecycleManager, ScopeMatcher $scopeMatcher, Environment $twig)
    {
        $this->lifecycleManager = $lifecycleManager;
        $this->scopeMatcher = $scopeMatcher;
        $this->twig = $twig;
    }

    protected function getResponse(Template $template, ContentModel $model, Request $request): ?Response
    {
        $formId = (int)$model->form;
        $debug = $this->getParameter('kernel.debug');

        if ($this->scopeMatcher->isBackendRequest($request)) {
            return new Response("### FORM (ID: $formId) ###");
        }

        if ($model->invisible) {
            return new Response();
        }

        $livewireCss = $this->twig->render('@SymfonyLivewire/styles.html.twig', ['debug' => $debug]);
        $livewireJs = $this->twig->render('@SymfonyLivewire/scripts.html.twig', [
            'debug' => $debug,
            'jsonEncodedOptions' => '',
            'appUrl' => $request->getSchemeAndHttpHost() . $request->getBaseUrl(),
        ]);
        if (!array_key_exists('TL_BODY', $GLOBALS)) {
            $GLOBALS['TL_BODY'] = [];
        }
        if (!array_key_exists('TL_HEAD', $GLOBALS)) {
            $GLOBALS['TL_HEAD'] = [];
        }
        if (!in_array($livewireCss, $GLOBALS['TL_HEAD'])) {
            $GLOBALS['TL_HEAD'][] = $livewireCss;
        }
        if (!in_array($livewireJs, $GLOBALS['TL_BODY'])) {
            $GLOBALS['TL_BODY'][] = $livewireJs;
        }

        [$cssID, $class] = StringUtil::deserialize($model->cssID);
        // @phpstan-ignore-next-line
        $successMessage = $model->dynamicFormsSuccess;

        return new Response($this->lifecycleManager->initialRequest('dynamic-form', $formId, $successMessage, $cssID, $class));
    }
}
