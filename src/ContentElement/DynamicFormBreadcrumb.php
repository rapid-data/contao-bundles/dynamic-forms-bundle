<?php
declare(strict_types=1);

namespace RapidData\ContaoDynamicFormsBundle\ContentElement;

use Contao\BackendTemplate;
use Contao\ContentModel;
use Contao\CoreBundle\Controller\ContentElement\AbstractContentElementController;
use Contao\CoreBundle\Routing\ScopeMatcher;
use Contao\CoreBundle\ServiceAnnotation\ContentElement;
use Contao\StringUtil;
use Contao\Template;
use RoflCopter24\SymfonyLivewireBundle\Manager\LifecycleManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class FormMultipageBreadcrumb
 * @package App\ContentElement
 * @ContentElement(category="form")
 */
class DynamicFormBreadcrumb extends AbstractContentElementController
{
    private LifecycleManager $lifecycleManager;
    private ScopeMatcher $scopeMatcher;

    public function __construct(LifecycleManager $lifecycleManager, ScopeMatcher $scopeMatcher)
    {
        $this->lifecycleManager = $lifecycleManager;
        $this->scopeMatcher = $scopeMatcher;
    }

    protected function getResponse(Template $template, ContentModel $model, Request $request): ?Response
    {
        $formId = $model->form;
        if ($this->scopeMatcher->isBackendRequest($request)) {
            $beTemplate = new BackendTemplate('be_wildcard');
            // @phpstan-ignore-next-line
            $beTemplate->wildcard = '### FORM BROTKRUMEN ###';
            return $beTemplate->getResponse();
        }

        if ($model->invisible) {
            return new Response();
        }

        [$cssID, $classes] = StringUtil::deserialize($model->cssID);
        // @phpstan-ignore-next-line
        if ($model->dynamicFormsBreadcrumbMode) {
            return new Response($this->lifecycleManager->initialRequest('dynamic-form-breadcrumb', (int)$formId, $cssID, $classes, $model->dynamicFormsBreadcrumbMode));
        }
        return new Response($this->lifecycleManager->initialRequest('dynamic-form-breadcrumb', (int)$formId, $cssID, $classes));
    }
}
