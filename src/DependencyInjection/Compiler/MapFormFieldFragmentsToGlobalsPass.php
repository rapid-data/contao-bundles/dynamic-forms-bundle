<?php
declare(strict_types = 1);

namespace RapidData\ContaoDynamicFormsBundle\DependencyInjection\Compiler;

use Contao\CoreBundle\EventListener\GlobalsMapListener;
use RapidData\ContaoDynamicFormsBundle\Fragment\FormFieldProxy;
use RapidData\ContaoDynamicFormsBundle\Fragment\Reference\FormFieldReference;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\Compiler\PriorityTaggedServiceTrait;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Definition;

class MapFormFieldFragmentsToGlobalsPass implements CompilerPassInterface
{
    use PriorityTaggedServiceTrait;

    /**
     *
     * @inheritDoc
     */
    public function process(ContainerBuilder $container): void
    {
        // collect all fragments tagged as form fields
        $tags = $this->getFragmentTags($container, FormFieldReference::TAG_NAME);
        // create a mapping definition to a proxy class for each fragment
        $formFields = $this->getGlobalsMap($tags, 'TL_FFL', FormFieldProxy::class);

        // add listener Definition to container to register the fragments and make the hooks work
        $listener = new Definition(GlobalsMapListener::class, [$formFields]);
        $listener->setPublic(true);
        $listener->addTag('contao.hook', ['hook' => 'initializeSystem', 'priority' => 255]);

        $container->setDefinition('contao.listener.'.ContainerBuilder::hash($listener), $listener);
    }

    /**
     * @param array  $tags
     * @param string $globalsKey
     * @param string $proxyClass
     * @return array<string,array<int|string,array<int|string,string>>>
     */
    private function getGlobalsMap(array $tags, string $globalsKey, string $proxyClass): array
    {
        $values = [];

        foreach ($tags as $attributes) {
            $values[$globalsKey][$attributes['type']] = $proxyClass;
        }
        // @phpstan-ignore-next-line
        return $values;
    }

    /**
     * @param ContainerBuilder $container
     * @param string           $tag
     * @return array<string>
     */
    private function getFragmentTags(ContainerBuilder $container, string $tag): array
    {
        $result = [];

        foreach ($this->findAndSortTaggedServices($tag, $container) as $reference) {
            // @phpstan-ignore-next-line
            $definition = $container->findDefinition($reference);

            foreach ($definition->getTag($tag) as $attributes) {
                if (!isset($attributes['type'])) {
                    throw new InvalidConfigurationException(sprintf('Missing type for "%s" fragment on service ID "%s"', $tag, (string) $reference));
                }

                $result[] = $attributes;
            }
        }

        return $result;
    }
}
