<?php
declare(strict_types = 1);

namespace RapidData\ContaoDynamicFormsBundle\ServiceAnnotation;

use Contao\CoreBundle\ServiceAnnotation\AbstractFragmentAnnotation;
use RapidData\ContaoDynamicFormsBundle\Fragment\Reference\FormFieldReference;

/**
 * Annotation to define a controller as content element.
 *
 * @Annotation
 * @Target({"CLASS", "METHOD"})
 * @Attributes({
 *     @Attribute("value", type = "string"),
 *     @Attribute("category", type = "string"),
 *     @Attribute("template", type = "string"),
 *     @Attribute("renderer", type = "string"),
 *     @Attribute("attributes", type = "array")
 * })
 * @package RapidData\ContaoDynamicFormsBundle\ServiceAnnotation
 */
final class FormField extends AbstractFragmentAnnotation
{
    public function getName(): string
    {
        return FormFieldReference::TAG_NAME;
    }
}
