<?php
declare(strict_types=1);

namespace RapidData\ContaoDynamicFormsBundle\Twig;

use Http\Client\Exception;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class EsiHelperExtension extends AbstractExtension
{
    private HttpClientInterface $httpClient;
    private LoggerInterface $logger;
    private string $baseUrl;

    public function __construct(HttpClientInterface $httpClient, LoggerInterface $logger, RequestStack $requestStack)
    {
        $this->httpClient = $httpClient;
        $this->logger = $logger;
        $request = $requestStack->getCurrentRequest();
        $this->baseUrl = $request ? $request->getSchemeAndHttpHost() : '';
    }

    public function getFilters(): array
    {
        return [
            new TwigFilter('resolve_esi', [$this, 'resolveEsi'], ['is_safe' => ['html']])
        ];
    }

    /**
     * Finds and resolves embedded ESI-Tags for use with AJAX-Requests.
     * @param string $input The string to check and resolve ESI-Tags fro.
     * @return string The content with the result of the ESI-Requests.
     */
    public function resolveEsi(string $input): string
    {
        return preg_replace_callback('/<esi:include[^>]*>/', function (array $matches): string {
            // for each esi tag
            return collect($matches)
                ->map(function (string $esiInclude): string {
                    $urls = [];
                    preg_match('/src\s*=\s*"(.+?)"/', $esiInclude, $urls);
                    try {
                        // fetch the content, the 2nd entry in the array is the content of the regex capture group
                        // that represents the url
                        $response = $this->httpClient->request('GET', $this->baseUrl . $urls[1]);
                        $status = $response->getStatusCode();

                        if ($status !== 200) {
                            $this->logger->warning("[DynamicForm] ESI-Lazyload failed with code $status!");
                            return '';
                        }

                        return $response->getContent(true);
                    } catch (Exception $exception) {
                        $this->logger->error($exception->getMessage(), $exception->getTrace());
                        return '';
                    }
                })
                ->join('');
        }, $input);
    }
}
