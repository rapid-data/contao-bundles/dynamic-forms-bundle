<?php
declare(strict_types=1);

namespace RapidData\ContaoDynamicFormsBundle\Widget;

use BadMethodCallException;
use Contao\BackendTemplate;
use Contao\FormFieldModel;
use Contao\Widget;

class PageSwitchWidget extends Widget
{
    /**
     * Template
     *
     * @var string
     */
    protected $strTemplate = 'form_page-switch';

    /**
     * The CSS class prefix
     *
     * @var string
     */
    protected $strPrefix = 'widget widget-pagebreak';

    /**
     * Submit indicator
     * @var boolean
     */
    protected $blnSubmitInput = false;

    /**
     * Do not validate this form field
     *
     * @param string $varInput
     *
     * @return string|null
     */
    public function validator($varInput): ?string
    {
        return $varInput;
    }

    /**
     * Add custom HTML after the widget
     *
     * @param null $arrAttributes
     *
     * @return string
     */
    // @phpstan-ignore-next-line
    public function parse($arrAttributes = null)
    {
        // @phpstan-ignore-next-line
        if (TL_MODE == 'BE') {
            $template = new BackendTemplate('be_wildcard');
            // @phpstan-ignore-next-line
            $template->wildcard = '### PAGE BREAK ' . ($this->pageTitle !== '' ? "für Seite \"$this->pageTitle\"" : '') .' ###';

            return $template->parse();
        }
        // @phpstan-ignore-next-line
        $this->canGoBack = !$this->isFirstStepForForm($this->pid);

        return parent::parse($arrAttributes);
    }

    /**
     * Old generate() method that must be implemented due to abstract declaration.
     *
     * @throws BadMethodCallException
     */
    public function generate()
    {
        throw new BadMethodCallException('Calling generate() has been deprecated, you must use parse() instead!');
    }

    private function isFirstStepForForm($pid): bool
    {
        return collect(FormFieldModel::findPublishedByPid($pid))
            ->filter(function(FormFieldModel $fieldModel) {
                return $fieldModel->type === 'page-switch';
            })
            ->first()
            ->id === $this->id;
    }
}
