<?php
declare(strict_types=1);

namespace RapidData\ContaoDynamicFormsBundle\Widget;

use BadMethodCallException;
use Contao\BackendTemplate;
use Contao\FormFieldModel;
use Contao\System;

class PreviewWidget extends \Contao\Widget
{
    /**
     * Template
     *
     * @var string
     */
    protected $strTemplate = 'form_preview';

    /**
     * The CSS class prefix
     *
     * @var string
     */
    protected $strPrefix = 'widget widget-preview';

    /**
     * The form values "fieldName" => "fieldValue"
     * @var array
     */
    public array $formValues = [];

    /**
     * The labels of the fields if existent "fieldName" => "fieldLabel"
     * @var array
     */
    public array $formLabels = [];

    /**
     * Old generate() method that must be implemented due to abstract declaration.
     *
     * @throws BadMethodCallException
     */
    public function generate()
    {
        throw new BadMethodCallException('Calling generate() has been deprecated, you must use parse() instead!');
    }

    /**
     * Do not validate this form field
     *
     * @param string $varInput
     *
     * @return string|null
     */
    public function validator($varInput): ?string
    {
        return $varInput;
    }

    /**
     * Add custom HTML after the widget
     *
     * @param null $arrAttributes
     *
     * @return string
     */
    // @phpstan-ignore-next-line
    public function parse($arrAttributes = null): string
    {
        // @phpstan-ignore-next-line
        if (TL_MODE == 'BE') {
            $template = new BackendTemplate('be_wildcard');
            // @phpstan-ignore-next-line
            $template->wildcard = '### VORSCHAU DER FORMWERTE ###';

            return $template->parse();
        }

        $formId = FormFieldModel::findById($this->id)->pid;
        $fieldModels = FormFieldModel::findPublishedByPid($formId)->getModels();
        $this->formValues = System::getContainer()->get('request_stack')->getCurrentRequest()->request->all();
        // @phpstan-ignore-next-line
        $this->formLabels = collect($fieldModels)
            ->filter->name
            ->mapWithKeys(function (FormFieldModel $fieldModel) {
                return [$fieldModel->name => $fieldModel->label];
            })
            ->toArray();

        return parent::parse($arrAttributes);
    }
}
