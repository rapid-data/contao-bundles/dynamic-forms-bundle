<?php
declare(strict_types = 1);

namespace RapidData\ContaoDynamicFormsBundle\FormField;

use Contao\FormFieldModel;
use Contao\Template;
use RapidData\ContaoDynamicFormsBundle\ServiceAnnotation\FormField;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PageSwitchFormField
 *
 * @FormField("dynaForm_pageSwitch", template="form_dynaform_page-switch", category="dynamicForms")
 * @package RapidData\ContaoDynamicFormsBundle\FormField
 */
class PageSwitchFormFieldController extends AbstractFormFieldController
{
    protected function getResponse(Template $template, FormFieldModel $model, Request $request): ?Response
    {
        dump($model);
        $template->setData([
            'canGoBack' => true,
            'name' => $model->name,
            // @phpstan-ignore-next-line
            'labelBtnBack' => $model->btnBack ?: $model->mp_forms_backButton,
            // @phpstan-ignore-next-line
            'labelBtnNext' => $model->btnNext ?: $model->slabel
        ]);
        return $template->getResponse();
    }
}
