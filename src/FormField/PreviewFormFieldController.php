<?php
declare(strict_types = 1);

namespace RapidData\ContaoDynamicFormsBundle\FormField;

use Contao\FormFieldModel;
use Contao\Template;
use RapidData\ContaoDynamicFormsBundle\ServiceAnnotation\FormField;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PageSwitchFormField
 *
 * @FormField("dynaForm_preview", template="dynaform_preview", category="dynaForm_preview")
 * @package RapidData\ContaoDynamicFormsBundle\FormField
 */
class PreviewFormFieldController extends AbstractFormFieldController
{
    protected function getResponse(Template $template, FormFieldModel $model, Request $request): ?Response
    {
        dump($model);
        $template->setData([
            'canGoBack' => true,
            'name' => $model->name,
            // @phpstan-ignore-next-line
            'labelBtnBack' => $model->labelBtnBack ?: $model->mp_forms_backButton,
            // @phpstan-ignore-next-line
            'labelBtnSubmit' => $model->labelBtnSubmit ?: $model->slabel
        ]);
        return $template->getResponse();
    }
}
