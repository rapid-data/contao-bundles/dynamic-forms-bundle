<?php
declare(strict_types = 1);

namespace RapidData\ContaoDynamicFormsBundle\FormField;

use Contao\CoreBundle\Controller\AbstractFragmentController;
use Contao\FormFieldModel;
use Contao\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AbstractFormFieldController
 *
 * @package RapidData\ContaoDynamicFormsBundle\FormField
 */
abstract class AbstractFormFieldController extends AbstractFragmentController
{
    public function __invoke(Request $request, FormFieldModel $model): Response
    {
        $type = $this->getType();
        $template = $this->createTemplate($model, 'form_'.$type);

        $this->tagResponse(['contao.db.tl_form_fields.'.$model->id]);

        $response = $this->getResponse($template, $model, $request);

        if (null === $response) {
            dump($this);
            $response = $template->getResponse();
        }

        return $response;
    }

    abstract protected function getResponse(Template $template, FormFieldModel $model, Request $request): ?Response;
}
