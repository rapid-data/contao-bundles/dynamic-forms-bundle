<?php
use RapidData\ContaoDynamicFormsBundle\Widget\PageSwitchWidget;
use RapidData\ContaoDynamicFormsBundle\Widget\PreviewWidget;

$GLOBALS['TL_FFL']['page-switch'] = PageSwitchWidget::class;
$GLOBALS['TL_FFL']['preview'] = PreviewWidget::class;
