<?php
$GLOBALS['TL_DCA']['tl_content']['palettes']['form_element'] = '{type_legend},type;{include_legend},form,dynamicFormsSuccess;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID;{invisible_legend:hide},invisible';
$GLOBALS['TL_DCA']['tl_content']['palettes']['dynamic_form_breadcrumb'] = '{type_legend},type;{include_legend},form,dynamicFormsBreadcrumbMode;{protected_legend:hide},protected;{expert_legend:hide},guests,cssID;{invisible_legend:hide},invisible';

$GLOBALS['TL_DCA']['tl_content']['fields']['dynamicFormsSuccess'] = [
    'exclude' => true,
    'search' => true,
    'inputType' => 'textarea',
    'eval' => [
        'mandatory' => true,
        'rte' => 'tinyMCE',
        'helpwizard' => true,
        'tl_class' => 'clr w100'
    ],
    'explanation' => 'insertTags',
    'sql' => 'mediumtext NULL'
];

$GLOBALS['TL_DCA']['tl_content']['fields']['dynamicFormsBreadcrumbMode'] = [
    'exclude' => true,
    'search' => false,
    'inputType' => 'select',
    'options' => [
        'breadcrumb',
        'progressbar'
    ],
    'reference' => &$GLOBALS['TL_LANG']['tl_content']['dynamicFormsBreadcrumbMode'],
    'eval' => [
        'mandatory' => true,
        'tl_class' => 'w50'
    ],
    'sql' => "varchar(64) NOT NULL default 'breadcrumb'"
];
