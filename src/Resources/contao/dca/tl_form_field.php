<?php

$GLOBALS['TL_DCA']['tl_form_field']['palettes']['dynaForm_pageSwitch'] = '{type_legend},type,name;{settings_legend},pageTitle,btnBack,btnNext;{expert_legend:hide},class;{template_legend:hide},customTpl;{invisible_legend:hide},invisible';
$GLOBALS['TL_DCA']['tl_form_field']['palettes']['dynaForm_preview'] = '{type_legend},type,headline;{expert_legend:hide},class;{template_legend:hide},customTpl';
$GLOBALS['TL_DCA']['tl_form_field']['palettes']['page-switch'] = '{type_legend},type,label;{settings_legend},pageTitle,btnBack,btnNext;{image_legend:hide},imageSubmit;{expert_legend:hide},class,accesskey,tabindex;{template_legend:hide},customTpl;{invisible_legend:hide},invisible';

$GLOBALS['TL_DCA']['tl_form_field']['fields']['btnBack'] = [
    'exclude' => true,
    'search' => true,
    'inputType' => 'text',
    'eval' => [
        'maxlength' => 255,
        'tl_class' => 'w50'
    ],
    'sql' => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_form_field']['fields']['btnNext'] = [
    'exclude' => true,
    'search' => true,
    'inputType' => 'text',
    'eval' => [
        'maxlength' => 255,
        'tl_class' => 'w50'
    ],
    'sql' => "varchar(255) NOT NULL default ''"
];

$GLOBALS['TL_DCA']['tl_form_field']['fields']['pageTitle'] = [
    'exclude' => true,
    'search' => true,
    'inputType' => 'text',
    'eval' => [
        'maxlength' => 255,
        'tl_class' => 'w50'
    ],
    'sql' => "varchar(255) NOT NULL default ''"
];
