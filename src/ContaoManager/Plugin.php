<?php
declare(strict_types = 1);

namespace RapidData\ContaoDynamicFormsBundle\ContaoManager;

use Contao\CoreBundle\ContaoCoreBundle;
use Contao\CoreBundle\DependencyInjection\Compiler\DataContainerCallbackPass;
use Contao\CoreBundle\DependencyInjection\Compiler\RegisterHookListenersPass;
use Contao\ManagerPlugin\Bundle\BundlePluginInterface;
use Contao\ManagerPlugin\Bundle\Config\BundleConfig;
use Contao\ManagerPlugin\Bundle\Parser\ParserInterface;
use Contao\ManagerPlugin\Config\ConfigPluginInterface;
use Contao\ManagerPlugin\Config\ContainerBuilder;
use Contao\ManagerPlugin\Routing\RoutingPluginInterface;
use HeimrichHannot\TwigSupportBundle\HeimrichHannotTwigSupportBundle;
use RapidData\CaptchaBundle\RapidDataCaptchaBundle;
use RapidData\ContaoDynamicFormsBundle\DependencyInjection\Compiler\MapFormFieldFragmentsToGlobalsPass;
use RapidData\ContaoDynamicFormsBundle\DependencyInjection\Compiler\RegisterFormFieldFragmentsPass;
use RapidData\ContaoDynamicFormsBundle\DynamicFormsBundle;
use RapidData\ContaoDynamicFormsBundle\Fragment\Reference\FormFieldReference;
use RoflCopter24\SymfonyLivewireBundle\SymfonyLivewireBundle;
use Symfony\Bundle\TwigBundle\TwigBundle;
use Symfony\Component\Config\Loader\LoaderInterface;
use Symfony\Component\Config\Loader\LoaderResolverInterface;
use Symfony\Component\HttpKernel\DependencyInjection\FragmentRendererPass;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\RouteCollection;
use Terminal42\ServiceAnnotationBundle\Terminal42ServiceAnnotationBundle;
use Twig\Extra\TwigExtraBundle\TwigExtraBundle;

class Plugin implements BundlePluginInterface, ConfigPluginInterface, RoutingPluginInterface
{

    /**
     * @inheritDoc
     */
    final public function getBundles(ParserInterface $parser): array
    {
        return [
            BundleConfig::create(SymfonyLivewireBundle::class)->setLoadAfter([TwigBundle::class, Terminal42ServiceAnnotationBundle::class]),
            BundleConfig::create(TwigExtraBundle::class)->setLoadAfter([TwigBundle::class]),
            BundleConfig::create(DynamicFormsBundle::class)
                ->setLoadAfter([
                    ContaoCoreBundle::class,
                    HeimrichHannotTwigSupportBundle::class,
                    SymfonyLivewireBundle::class,
                    RapidDataCaptchaBundle::class

                ])
        ];
    }

    public function getRouteCollection(LoaderResolverInterface $resolver, KernelInterface $kernel): ?RouteCollection
    {
        $routingFile = __DIR__.'/../Resources/config/routing.yml';

        return $resolver->resolve($routingFile)->load($routingFile);
    }


    final public function registerContainerConfiguration(LoaderInterface $loader, array $managerConfig): void
    {
        $loader->load(static function (ContainerBuilder $container) {
            //$container->addCompilerPass(new RegisterFormFieldFragmentsPass(FormFieldReference::TAG_NAME, FormFieldReference::GLOBALS_KEY, FormFieldReference::PROXY_CLASS));
            //$container->addCompilerPass(new MapFormFieldFragmentsToGlobalsPass());
            //$container->addCompilerPass(new FragmentRendererPass('contao.fragment.handler'));
            /*$container->addCompilerPass(new DataContainerCallbackPass());
            $container->addCompilerPass(new RegisterHookListenersPass(), PassConfig::TYPE_OPTIMIZE);*/
        });
    }
}
