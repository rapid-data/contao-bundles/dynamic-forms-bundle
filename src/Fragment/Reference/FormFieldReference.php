<?php
declare(strict_types = 1);

namespace RapidData\ContaoDynamicFormsBundle\Fragment\Reference;

use Contao\CoreBundle\Fragment\Reference\FragmentReference;
use Contao\FormFieldModel;
use RapidData\ContaoDynamicFormsBundle\Fragment\FormFieldProxy;

class FormFieldReference extends FragmentReference
{
    public const TAG_NAME = 'contao.form_field';
    public const GLOBALS_KEY = 'TL_FFL';
    public const PROXY_CLASS = FormFieldProxy::class;

    public function __construct(FormFieldModel $model)
    {
        parent::__construct(self::TAG_NAME.'.'.$model->type);

        $this->attributes['formFieldModel'] = $model->id;
    }
}
